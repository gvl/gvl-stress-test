import sys
import argparse
import string
import os
import urllib2
from threading import Thread
import logging

from fabric.api import get
from fabric.state import env
from fabric.network import disconnect_all
from concurrent import futures
"""
Runs a series of workloads through selenium grid, simulating an increasing number of users each time.
"""
import unittest
import time
from bioblend.cloudman.launch import Bunch
from bioblend.cloudman import CloudManConfig
from bioblend.cloudman import CloudManInstance
import gvl_test_runner

LOG_CONFIG = {'version':1,
              'formatters': {
                            'default_formatter' : { 'format': '%(asctime)s : %(levelname)s : %(message)s' }
                            },
              'handlers': {'console': { 'class': 'logging.StreamHandler',
                                      'level': logging.DEBUG},
                          'file': {'class': 'logging.FileHandler',
                                   'filename': 'benchmark.log',
                                   'formatter': 'default_formatter',
                                   'level': logging.DEBUG}},
              'root': {'handlers' : ('console', 'file'), 'level': logging.DEBUG }}
logging.config.dictConfig(LOG_CONFIG)
log = logging.getLogger(__name__)

DEFAULT_MACHINE_TYPE = "m1.medium"
DEFAULT_PLACEMENT_ZONE = 'melbourne-qh2'
DEFAULT_GALAXY_DATA_STORAGE = "transient"
DEFAULT_GALAXY_INDEX_STORAGE = "gluster"
DEFAULT_NUM_WORKERS = 5
DEFAULT_PASSWORD = 'gvl_letmein'
DEFAULT_AMI_ID = "ami-00001de9"  # GVL-2.12-Customised-For-Performance-Test
# DEFAULT_AMI_ID = "ami-00001dbc"  # GVL 2.12
DEFAULT_GRID_SERVER = "http://115.146.86.155:5555/wd/hub"
DEFAULT_CM_BUCKET = "cloudman-os"

LIST_WORKLOADS = ["tests.workflow_deseq_basic.WorkflowDeseqBasic", "tests.workflow_microbial_assembly.WorkflowMicrobialAssembly", "tests.workflow_variant_detection_basic.WorkflowVariantDetectionBasic", "tests.rnaseq_dge_basic_prep.RNAseqDGEBASICPrep"]
# LIST_WORKLOADS = ["tests.workflow_microbial_assembly.WorkflowMicrobialAssembly", "tests.workflow_variant_detection_basic.WorkflowVariantDetectionBasic", "tests.rnaseq_dge_basic_prep.RNAseqDGEBASICPrep"]
# LIST_NUM_USERS = [1, 5, 10, 20, 30]
LIST_NUM_USERS = [1, 5, 10, 20]
# LIST_NUM_USERS = [1]
DEFAULT_MAX_WAIT_TILL_WORKERS_READY = 600
MAX_RETRIES = 2

def launch_cm_instance(zone, machine_type, ami_id, akey, skey, gdata_storage, gindex_storage, workload, user, total_workers, bucket):
    access_key = akey
    secret_key = skey
    cloud_metadata = Bunch(id='-1',
                           name="nectar" if gindex_storage == "gluster" else "nectar_melbourne_dev",
                           cloud_type='openstack',
                           bucket_default=bucket,
                           region_name='melbourne',
                           region_endpoint='nova.rc.nectar.org.au',
                           ec2_port=8773,
                           ec2_conn_path='/services/Cloud',
                           cidr_range='115.146.92.0/22',
                           is_secure=True,
                           s3_host='swift.rc.nectar.org.au',
                           s3_port=8888,
                           s3_conn_path='/')
    cluster_name = 'GVL_{0}_{1}_{2}_{3}_u{4}_w{5}'.format(machine_type, gdata_storage, gindex_storage, workload, user, total_workers)
    ami_id = ami_id
    instance_type = machine_type
    password = DEFAULT_PASSWORD
    data_option = gdata_storage if gdata_storage else 'transient'
    cfg = CloudManConfig(access_key, secret_key, cluster_name + "-" + str(time.time()), ami_id, instance_type, password, cloud_metadata=cloud_metadata, cluster_type='Galaxy', galaxy_data_option=data_option, initial_storage_size=100, block_till_ready=True, placement=zone)
    cmi = CloudManInstance.launch_instance(cfg)
    # TODO: Hack to clean up cluster name
    cmi.config.cluster_name = cluster_name
    status = cmi.get_status()
    if 'ERROR' in status['cluster_status']:
        raise Exception("instance.get_status() returned ERROR. Should return a successful status!")
    log.info("Master instance launched successfully. Instance type: {0}, Galaxy Data:: {1}, Galaxy Index: {2}".format(machine_type, gdata_storage, gindex_storage))
    return cmi

def add_cm_worker(cmi, total_workers):
    status = cmi.add_nodes(1)
    availabe_workers = int(status['instance_status']['available'])
    current_wait = 0
    retries = 0
    while availabe_workers < total_workers:
        if retries >= MAX_RETRIES:
            raise Exception('Retried {0} times to get {0} workers but did not succeed. Aborting...'.format(retries, total_workers))
        if current_wait > DEFAULT_MAX_WAIT_TILL_WORKERS_READY:
            log.info('Could not get {0} workers after a reasonable wait. Removing the last worker and retrying'.format(total_workers))
            retries += 1
            status = cmi.remove_nodes(1)
            status = cmi.add_nodes(1)
            current_wait = 0
        log.info('Waiting for {0} workers to be available. Current state: {1}'.format(total_workers, status['instance_status']))
        time.sleep(30)
        current_wait += 30
        status = cmi.get_status()
        try:
            availabe_workers = int(status['instance_status']['available'])
        except Exception, e:
            log.exception("Error getting number of available workers. Retrying...")
            retries += 1

    log.info('Successfully added {0} workers'.format(total_workers))
    # cmi.enable_master_as_execution_host(True)

def add_cm_workers(cmi, total_workers):
    log.info("Iteratively adding workers. Requested: {0}".format(total_workers))
    for worker in range(1, total_workers + 1):
        log.info("Adding worker: {0}".format(worker))
        add_cm_worker(cmi, worker)

def is_galaxy_running(url):
    running_error_codes = [403]
    # Error codes that indicate Galaxy is running
    try:
        urllib2.urlopen(url)
        return True
    except urllib2.HTTPError, e:
        return e.code in running_error_codes
    except:
        return False

def wait_till_galaxy_ready(cm_instance):
    current_wait = 0
    log.info("Checking whether galaxy is running at {0}...".format(cm_instance.galaxy_url))
    while not is_galaxy_running(cm_instance.galaxy_url):
        if current_wait > DEFAULT_MAX_WAIT_TILL_WORKERS_READY:
            raise Exception('Galaxy server at {0} not responding after maximum wait. Aborting...'.format(cm_instance.galaxy_url))
        time.sleep(30)
        current_wait += 30

def run_workload(grid, thread_no, cm_instance, workload, total_users):
    log_file_name = "logs/{0}_thread_{1}".format(cm_instance.config.cluster_name, thread_no)
    selenium_server = grid
    galaxy_server = cm_instance.galaxy_url
    username = gvl_test_runner.DEFAULT_GALAXY_USERNAME + str(thread_no)
    email = str(thread_no) + gvl_test_runner.DEFAULT_GALAXY_EMAIL
    password = gvl_test_runner.DEFAULT_GALAXY_PASSWORD
    page_timeout = gvl_test_runner.DEFAULT_PAGE_TIMEOUT
    job_timeout = 5400  # gvl_test_runner.DEFAULT_JOB_TIMEOUT
    fqcn = workload
    log.info("Launching runner no: {0} with workload: {1}".format(thread_no, workload))
    try:
        gvl_test_runner.run_test(log_file_name, selenium_server, galaxy_server, username, email, password, page_timeout, job_timeout, fqcn)
    except Exception, e:
        log.exception("An error occurred while executing test:")
        raise e
    return True

def simulate_n_user_workload(grid, cm_instance, workload, num_users):
    if workload == "tests.rnaseq_dge_basic_prep.RNAseqDGEBASICPrep" or workload == "tests.rnaseq_dge_advanced_prep.RNAseqDGEAdvancedPrep":
        try:
            # hardcode thread no to differentiate
            run_workload(grid, 0xDEAD, cm_instance, "tests.import_tutorial_histories.ImportTutorialHistories", num_users)
        except:
            log.exception("Could not import tutorial histories. Aborting...")

    assert(num_users >= 1 and num_users < 100)

    log.info("Spawning workload runner threads...")
    with futures.ProcessPoolExecutor(max_workers=num_users) as executor:
        # Execute the copy on a separate thread,
        # creating a future object to track progress.
        threads = [executor.submit(run_workload, grid, x, cm_instance, workload, num_users) for x in range(0, num_users)]
        futures.wait(threads, return_when=futures.ALL_COMPLETED)
        for t in threads:
            # Check the value returned by each test.
            # Raise any exceptions raised during test execution.
            t.result()

def archive_logs(node_num, node_ip, log_dir):
    log.info("Archiving logs on node: {0}...".format(node_ip))
    env.host_string = node_ip
    env.user = "ubuntu"
    env.password = DEFAULT_PASSWORD
    env.disable_known_hosts = True
    target_dir = os.path.join(log_dir, "worker-{0}".format(node_num))
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    get("/var/log/atop/*", target_dir)
    disconnect_all()

def archive_all_logs(cm_instance, log_dir):
    log.info("Archiving master and worker logs...")
    nodes = cm_instance.get_nodes()
    for idx, node in enumerate(nodes):
        archive_logs(idx, node['public_ip'], log_dir)

def execute_n_worker_test(grid, zone, machine_type, ami_id, akey, skey, gdata_storage, gindex_storage, workload, user, worker_num, bucket):
    log.info("Launching master instance of type: {0} with {1} workers.".format(machine_type, worker_num))
    cm_instance = launch_cm_instance(zone, machine_type, ami_id, akey, skey, gdata_storage, gindex_storage, workload, user, worker_num, bucket)

    try:
        log_dir = "logs/logs-{0}".format(cm_instance.config.cluster_name)
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        if worker_num > 0:
            add_cm_workers(cm_instance, worker_num)

        wait_till_galaxy_ready(cm_instance)

        start_time = time.time()
        simulate_n_user_workload(grid, cm_instance, workload, user)
        end_time = time.time()
        total_time = end_time - start_time
        result = "workload={0},time_taken={1},workers={2},users={3},galaxy_data={4},galaxy_index={5},instance_type={6},start_time={7},end_time={8}\n".format(workload, total_time, worker_num, user, gdata_storage, gindex_storage, machine_type, start_time, end_time)
        log.info(result)

        log_file_name = "{0}/timing.log".format(log_dir)
        with open(log_file_name, "a") as text_file:
            text_file.write(result)
        retries = 0
        while True:
            if retries >= MAX_RETRIES:
                log.exception('Could not archive logs after: {0} attempts. Aborting...'.format(retries))
                break;
            try:
                archive_all_logs(cm_instance, log_dir)
                break;
            except Exception, e:
                log.exception("Exception while archiving logs... Retrying...")
                retries += 1
    finally:
        try:
            log.info("Terminating cluster: {0}".format(cm_instance.config.cluster_name))
            cm_instance.terminate(delete_cluster=True)
        except Exception, e:
            log.error("Exception while terminating: {0}".format(e))


def run_benchmark(grid, zone, machine_type, ami_id, akey, skey, total_workers, gdata_storage, gindex_storage, bucket,
                  user_start=0, workload_start=0, worker_start=0, user_end=None, workload_end=None, worker_end=None):
    for idx_work, workload in enumerate(LIST_WORKLOADS):

        if idx_work < workload_start:
            log.info("Skipping over workload: {0} on request".format(workload))
            continue
        elif workload_end and idx_work > workload_end:
            log.info("Finishing up after reaching workload: {0} on request".format(workload))
            return

        for user in LIST_NUM_USERS:
            if idx_work == workload_start and user < user_start:
                log.info("Skipping over workload: {0} user: {1} on request".format(workload, user))
                continue
            elif workload_end and user_end and idx_work == workload_end and user > user_end:
                log.info("Finishing up after reaching workload: {0} user: {1} on request".format(workload, user))
                return

            log.info("Testing workload: {0} with {1} users and a max of {2} workers".format(workload, user, total_workers))

            for worker_num in range(0, total_workers + 1):

                if idx_work == workload_start and user == user_start and worker_num < worker_start:
                    log.info("Skipping over workload: {0}, user: {1}, worker: {2} on request".format(user, workload, worker_num))
                    continue
                elif workload_end and user_end and worker_end and idx_work == workload_end and user == user_end and worker_num > worker_end:
                    log.info("Finishing up after reaching workload: {0}, user: {1}, worker: {2} on request".format(user, workload, worker_num))
                    return

                # Prune some unnecessary tests
                if user == 1 and worker_num > 2:
                    log.info("Skipping testing with more than two workers for a single user as the difference will be marginal.")
                    continue

                if worker_num > total_workers:
                    break;

                retries = 0
                while True:
                    if retries >= MAX_RETRIES:
                        log.error('Could not get a successful result for scenario: {0}-{1}-{2}-{3} after: {4} attempts. Skipping to next scenario...'.format(machine_type, workload, user, worker_num, retries))
                        break
                    try:
                        execute_n_worker_test(grid, zone, machine_type, ami_id, akey, skey, gdata_storage, gindex_storage, workload, user, worker_num, bucket)
                        break;
                    except Exception, e:
                        log.exception("Exception while executing test:")
                        retries += 1
                        log.info("Retry attempt: {0}".format(retries))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-ak", "--akey", type=str, help="Access key to use", required=True)
    parser.add_argument("-sk", "--skey", type=str, help="Secret key to use", required=True)
    parser.add_argument("-m", "--machine", type=str, default=DEFAULT_MACHINE_TYPE, help="Instance type/flavor to use (e.g. m1.small, m1.medium etc). Default is %s" % DEFAULT_MACHINE_TYPE)
    parser.add_argument("-w", "--workers", type=int, default=DEFAULT_NUM_WORKERS, help="Maximum number of workers to test. Default is %s" % DEFAULT_NUM_WORKERS)
    parser.add_argument("-d", "--galaxydata", type=str, default=DEFAULT_GALAXY_DATA_STORAGE, help="Storage to use for galaxy data. Should be either transient, or custom-size. Default is %s" % DEFAULT_GALAXY_DATA_STORAGE)
    parser.add_argument("-idx", "--galaxyindices", type=str, default=DEFAULT_GALAXY_INDEX_STORAGE, help="Storage to use for galaxy index. Should be either volume or gluster. Default is %s" % DEFAULT_GALAXY_INDEX_STORAGE)
    parser.add_argument("-img", "--image", type=str, default=DEFAULT_AMI_ID, help="AMI id to use. Default is %s" % DEFAULT_AMI_ID)
    parser.add_argument("-z", "--zone", type=str, default=DEFAULT_PLACEMENT_ZONE, help="Placement zone to use. Default is %s" % DEFAULT_PLACEMENT_ZONE)
    parser.add_argument("-gr", "--grid", default=DEFAULT_GRID_SERVER, help="Use remote selenium grid server. e.g. http://115.146.86.155:5555/wd/hub. Default is: %s" % DEFAULT_GRID_SERVER)
    parser.add_argument("-b", "--bucket", type=str, default=DEFAULT_CM_BUCKET, help="Default cloudman bucket to use. Default is: %s" % DEFAULT_CM_BUCKET)
    parser.add_argument("-s", "--skipstart", type=int, nargs=3, default=[0, 0, 0], help="Skip tests to desired start point. 3 values must be provided. e.g. --skiptest 2 0 3 would start running tests from users=2, workload=0, workers=3")
    parser.add_argument("-e", "--skipend", type=int, nargs=3, default=[None, None, None], help="Skip tests till desired end point. 3 values must be provided. e.g. --skipend 2 0 3 would start running tests till users=2, workload=0, workers=3")
    args = parser.parse_args()

    run_benchmark(args.grid, args.zone, args.machine, args.image, args.akey, args.skey, args.workers, args.galaxydata, args.galaxyindices, args.bucket,
                  user_start=args.skipstart[0], workload_start=args.skipstart[1], worker_start=args.skipstart[2],
                  user_end=args.skipend[0], workload_end=args.skipend[1], worker_end=args.skipend[2])
