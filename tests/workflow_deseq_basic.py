from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from gvl_test_base import GVLTestBase
from selenium_snippets.galaxy.history import History
from selenium_snippets.galaxy.get_data import GetData
from selenium_snippets.galaxy.workflow import Workflow
from selenium_snippets.galaxy import snippet_base

INPUT_FILE1 = "https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/RNAseqDGE_BASIC/C1_R3.chr4.fq"
INPUT_FILE2 = "https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/RNAseqDGE_BASIC/C2_R1.chr4.fq"
INPUT_FILE3 = "https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/RNAseqDGE_BASIC/ensembl_dm3.chr4.gtf"
WORKFLOW_URL = "https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/galaxy_tools_tests/test_workflow_for_DESeq_Basic_tut"
WORKFLOW_NAME = "workflow for DESeq Basic"
HISTORY_NAME = "TestBot: Deseq Tutorial Basic"

class WorkflowDeseqBasic(GVLTestBase):

    def __init__(self, galaxy_test_context):
        super(WorkflowDeseqBasic, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    def execute_gvl_testcase(self):
        self.import_test_data()
        self.import_and_run_workflow()
        self.verify_output()

    @snippet_base.ui_action()
    def import_test_data(self):
        driver = self.driver
        History(self.context).create_new_history(HISTORY_NAME)
        GetData(self.context).run_upload_file('url', INPUT_FILE1, 'fastqsanger')
        GetData(self.context).run_upload_file('url', INPUT_FILE2, 'fastqsanger')
        GetData(self.context).run_upload_file('url', INPUT_FILE3)
        History(self.context).wait_for_datasets_to_finish()

    @snippet_base.ui_action()
    def import_and_run_workflow(self):
        workflow = Workflow(self.context)
        workflow.import_workflow('url', WORKFLOW_URL)
        workflow.run_workflow(WORKFLOW_NAME, [INPUT_FILE1, INPUT_FILE2, INPUT_FILE3])
        History(self.context).wait_for_datasets_to_finish()

    @snippet_base.ui_action()
    def verify_output(self):
        driver = self.driver
        History(self.context).view_last_history_item()
        self.switch_to_galaxy_content_frame()
        if not "test_id gene_id gene locus sample_1 sample_2 status value_1 value_2 log2(fold_change) test_stat p_value q_value significant" in driver.find_element_by_xpath("id('content_table')/tbody/tr").text:
            raise Exception("Expected table column names not found")
        if not "FBgn0039902 FBgn0039902 CG2177 chr4:331557-334534 None None OK" in driver.find_element_by_xpath("id('content_table')/tbody/tr[2]").text:
            raise Exception("Did not find expected output values: FBgn0039902 FBgn0039902 CG2177 chr4:331557-334534 None None OK")

