import time
import os
import boto
import argparse
import logging
from fabric.api import *
from fabric.tasks import execute

import deploy_xtreemfs
import create_nodes as nodectrl

LOG_CONFIG = {'version':1,
              'formatters': {
                            'default_formatter' : { 'format': '%(asctime)s : %(levelname)s : %(message)s' }
                            },
              'handlers': {'console': { 'class': 'logging.StreamHandler',
                                      'level': logging.DEBUG},
                          'file': {'class': 'logging.FileHandler',
                                   'filename': 'create_node.log',
                                   'formatter': 'default_formatter',
                                   'level': logging.DEBUG}},
              'root': {'handlers' : ('console',), 'level': logging.DEBUG }}
logging.config.dictConfig(LOG_CONFIG)
log = logging.getLogger(__name__)

def create_xtreemfs_grid(hub_flavour, node_flavour, zones, instance_prefix, instances_per_zone, volume_size):
    log.debug("Creating xtreemfs Metadata/directory server (MRC - Meatadata and Replica Catalogue + DIR - Directory Service) in zone: {0}".format(zones[0]))
    hub = nodectrl.create_instance(hub_flavour, zones[0], instance_prefix + "-hub")
    osd_instances = []
    for zone in zones:
        log.debug("Creating {0} xtreemfs storage servers (OSD - Object Storage Device) in zone: {1}".format(instances_per_zone, zone))
        instances = nodectrl.create_instances(node_flavour, zone, instance_prefix + "-" + zone.lower() + "-osd{0}", instances_per_zone, wait_till_ready=False)
        log.debug("Waiting for instances to come up...")
        instances = nodectrl.wait_till_instances_ready(instances)
        osd_instances += instances
    hub = nodectrl.wait_till_instances_ready([hub])[0]
    log.debug("Creating and attaching volumes...")
    nodectrl.attach_volume(hub, "{0}-hub-vol".format(instance_prefix), volume_size, zones[0])
    for zone_idx, zone in enumerate(zones):
        for inst_no in xrange(0, instances_per_zone):
            log.debug("Attaching volume: {0} in zone: {1}".format(osd_instances[zone_idx * instances_per_zone + inst_no], zone))
            nodectrl.attach_volume(osd_instances[zone_idx * instances_per_zone + inst_no], "{0}-{1}-vol{2}".format(instance_prefix, zone.lower(), inst_no), volume_size, zone)
    log.debug("Sleeping a bit till the ssh services come up...")
    time.sleep(20)  # Sleep a little till the ssh services come up
    log.debug("Deploying xtreemfs software...")
    deploy_xtreemfs.deploy_xtreemfs_grid(hub.private_ips[0], [instance.private_ips[0] for instance in osd_instances])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-z', '--zones', nargs='+', type=str, help="List of placement zones to create nodes in", required=True)
    parser.add_argument('-n', '--nodes', type=int, help="Number of nodes per zone", required=True)
    parser.add_argument('-k', '--keyfile', type=str, help="Keyfile location", required=True)
    parser.add_argument('-m', '--master', type=str, help="Flavour of DIR/MRC node", required=True)
    parser.add_argument('-f', '--flavour', type=str, help="Flavour of nodes", required=True)
    parser.add_argument('-p', '--prefix', type=str, help="Name prefix of grid.", required=True)
    parser.add_argument('-s', '--size', type=int, help="Volume size of each peer node", required=True)
    args = parser.parse_args()

    env.user = "ubuntu"
    env.disable_known_hosts = True
    env.key_filename = args.keyfile
    create_xtreemfs_grid(args.master, args.flavour, args.zones, args.prefix, args.nodes, args.size)
