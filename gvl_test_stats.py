import sys
import unittest
from threading import Thread
import json
import argparse

SLOW_RESPONSE_THRESHOLD = 10  # Maximum time (in seconds) for a response before it's considered slow
DEFAULT_LOG_FILENAME = "gvltest"  # if no log file specified on command line as second argument

class ColorIO:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    @staticmethod
    def warn(str):
        return ColorIO.WARNING + str + ColorIO.ENDC

    @staticmethod
    def ok(str):
        return ColorIO.OKGREEN + str + ColorIO.ENDC

    @staticmethod
    def success(str):
        return ColorIO.OKBLUE + str + ColorIO.ENDC

    @staticmethod
    def fail(str):
        return ColorIO.FAIL + str + ColorIO.ENDC

    @staticmethod
    def heading(str):
        return ColorIO.HEADER + str + ColorIO.ENDC


class GVLTestStats():

    def print_stats(self, log_data, error_count, total, slowest, slow_percentage):
        print "\n-----------------------"
        print "GVL Stress Test Results"
        print "-----------------------"

        for index, run in enumerate(log_data):
            self.print_timing(index, run)

        print ("\nTotal timeouts/errors: {0}/{1}".format(error_count, total))
        print ("Slowest response: {0}".format(int(slowest)))
        print ("Percentage of responses > {0} secs: {1}".format(SLOW_RESPONSE_THRESHOLD, int(slow_percentage)))

    def load_log_data(self, log_file):
        log_data = []
        count = 0
        while True:
            filename = "{0}{1}.log".format(log_file_name, count)
            try:
                fp = open(filename, 'r')
                log_data.append(json.load(fp))
            except Exception, e:
                print e
                break
            count += 1
        return log_data

    def process_stats(self, log_file):
        log_data = self.load_log_data(log_file)

        error_count = 0
        total = 0
        total_actions = 0
        total_slow_actions = 0
        slowest_action = 0

        for index, run in enumerate(log_data):
            self.update_time_taken(run)
            error_count += run['errors']
            self.print_timing(index, run)
            total = index + 1
            run_slow_total, slowest, run_total = self.calculate_run_slowness(run)
            total_slow_actions += run_slow_total
            total_actions += run_total
            slowest_action = max(slowest_action, slowest)

        slow_percentage = (total_slow_actions * 100.0) / (total_actions * 1.0)
        self.print_stats(log_data, error_count, total, slowest_action, slow_percentage)

#     def print_stats(self, log_data, error_count, total, slowest, slow_percentage):
#         print "\n-----------------------"
#         print "GVL Stress Test Results"
#         print "-----------------------"
#
#         for index, run in enumerate(log_data):
#             self.print_timing(index, run)
#
#         print ("\nTotal timeouts/errors: {0}/{1}".format(error_count, total))
#         print ("Slowest response: {0}".format(int(slowest)))
#         print ("Percentage of responses > {0} secs: {1}".format(SLOW_RESPONSE_THRESHOLD, int(slow_percentage)))

#     def update_time_taken(self, run):
#         for count, action in enumerate(run['timing']):
#             if action.get('name') == 'finish':
#                 break
#             next_action = run['timing'][count + 1]
#             action['time_taken'] = next_action['timestamp'] - action['timestamp']
#
#     def print_timing(self, run_no, run):
#         print '\nBrowser: %s' % (run_no + 1)
#         print '{0:<5}{1:<40}{2:<15}'.format('no', 'action', 'time')
#         print '{0:<5}{1:<40}{2:<15}'.format('----', '---------------', '---------------')
#         for count, action in enumerate(run['timing']):
#             if action.get('name') == 'finish':
#                 break
#             time_taken = action['time_taken']
#             output = '{0:>3}. {1:<40}{2:>10.2f}'.format(count + 1, action['name'], time_taken)
#             if ('page load' in action['name'] or 'click' in action['name']):
#                 if time_taken > SLOW_RESPONSE_THRESHOLD:
#                     print ColorIO.fail(output)
#                 else:
#                     print ColorIO.ok(output)
#             else:
#                 print output
#
#     def calculate_run_slowness(self, run):
#         slow_count = 0
#         slowest = 0
#         total = 0
#         for count, action in enumerate(run['timing']):
#             if action.get('name') == 'finish':
#                 break
#             if ('page load' in action['name'] or 'click' in action['name']) and  action['time_taken'] > SLOW_RESPONSE_THRESHOLD:
#                 slow_count += 1
#             total = count + 1
#             slowest = max(slowest, action['time_taken'])
#         return (slow_count, slowest, total)
#
#     def process_stats(self, log_file):
#         log_data = self.load_log_data(log_file)
#
#         error_count = 0
#         total = 0
#         total_actions = 0
#         total_slow_actions = 0
#         slowest_action = 0
#
#         for index, run in enumerate(log_data):
#             self.update_time_taken(run)
#             error_count += run['errors']
#             self.print_timing(index, run)
#             total = index + 1
#             run_slow_total, slowest, run_total = self.calculate_run_slowness(run)
#             total_slow_actions += run_slow_total
#             total_actions += run_total
#             slowest_action = max(slowest_action, slowest)
#
#         slow_percentage = (total_slow_actions * 100.0) / (total_actions * 1.0)
#         self.print_stats(log_data, error_count, total, slowest_action, slow_percentage)
#
#     def load_log_data(self, log_file):
#         log_data = []
#         count = 0
#         while True:
#             filename = "{0}{1}.log".format(log_file_name, count)
#             try:
#                 fp = open(filename, 'r')
#                 log_data.append(json.load(fp))
#             except Exception, e:
#                 print e
#                 break
#             count += 1
#         return log_data

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--log", default=DEFAULT_LOG_FILENAME, help="Prefix for log files. Default is %s" % DEFAULT_LOG_FILENAME)
    parser.add_argument("-s", "--slow", default=SLOW_RESPONSE_THRESHOLD, help="Maximum time (in seconds) for a response before it's considered slow. Default is %s" % SLOW_RESPONSE_THRESHOLD)
    args = parser.parse_args()
    log_file_name = args.log
    SLOW_RESPONSE_THRESHOLD = args.slow

    assert(log_file_name)
    stat_printer = GVLTestStats()
    stat_printer.process_stats(log_file_name)
