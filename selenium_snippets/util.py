from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


def select_by_partial_text(select_element, text):
    sel = Select(select_element)
    options = sel.options

    xpath = ".//option[contains(., '%s')]" % text
    opts = sel._el.find_elements(By.XPATH, xpath)

    matched = False
    for opt in opts:
        sel._setSelected(opt)
        matched = True

    if matched != True:
        raise NoSuchElementException("Could not locate element with visible text: %s" % text)

def select_by_index_no(select_element, index):
    options = Select(select_element).options
    Select(select_element)._setSelected(options[index])
