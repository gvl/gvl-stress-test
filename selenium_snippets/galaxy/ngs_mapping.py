from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets.galaxy import snippet_base
from selenium_snippets import util

class NGSMapping(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(NGSMapping, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_ngs:_mapping", "Map with BWA for Illumina")
    def run_bwa_illumina(self, ref_genome_source, ref_genome, paired_end=None, fastq1=None, fastq2=None, extra_settings=None):
        driver = self.driver
        self.clock_action("bwa_illumina run_tool", 1001)
        self.switch_to_galaxy_content_frame()

        util.select_by_partial_text(driver.find_element_by_name("genomeSource|refGenomeSource"), ref_genome_source)
        util.select_by_partial_text(driver.find_element_by_name("genomeSource|indices"), ref_genome)

        if paired_end:
            util.select_by_partial_text(driver.find_element_by_name("paired|sPaired"), "Paired-end")
            util.select_by_partial_text(driver.find_element_by_name("paired|input1"), fastq1)
            util.select_by_partial_text(driver.find_element_by_name("paired|input2"), fastq2)
        else:
            util.select_by_partial_text(driver.find_element_by_name("paired|sPaired"), "Single-end")
            util.select_by_partial_text(driver.find_element_by_name("paired|input1"), fastq1)

        if extra_settings:
            util.select_by_partial_text(driver.find_element_by_name("params|source_select"), "Full Parameter List")
            if extra_settings['read_group']:
                util.select_by_partial_text(driver.find_element_by_name("params|readGroup|specReadGroup"), "Yes")
                driver.find_element_by_name("params|readGroup|rglb").clear()
                driver.find_element_by_name("params|readGroup|rglb").send_keys(extra_settings['read_group']['library'])
                driver.find_element_by_name("params|readGroup|rgpl").clear()
                driver.find_element_by_name("params|readGroup|rgpl").send_keys(extra_settings['read_group']['platform'])
                driver.find_element_by_name("params|readGroup|rgsm").clear()
                driver.find_element_by_name("params|readGroup|rgsm").send_keys(extra_settings['read_group']['sample'])
                if extra_settings['read_group']['id']:
                    driver.find_element_by_name("params|readGroup|rgid").clear()
                    driver.find_element_by_name("params|readGroup|rgid").send_keys(extra_settings['read_group']['id'])

        driver.find_element_by_name("runtool_btn").click()
