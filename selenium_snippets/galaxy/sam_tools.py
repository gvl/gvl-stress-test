from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets import util
from selenium_snippets.galaxy import snippet_base


class SamTools(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(SamTools, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_samtools", "BAM-to-SAM")
    def run_bam_to_sam(self, file_to_convert, include_header=False):
        driver = self.driver
        self.clock_action("BamToSam: " + file_to_convert + " run_tool", 1001)
        self.switch_to_galaxy_content_frame()
        util.select_by_partial_text(driver.find_element_by_name("input1"), file_to_convert)
        util.select_by_partial_text(driver.find_element_by_name("input1"), file_to_convert)
        header_checkbox = driver.find_element_by_id("header")
        if include_header and not header_checkbox.is_selected():
            header_checkbox.click()
        elif not include_header and header_checkbox.is_selected():
            header_checkbox.click()

        driver.find_element_by_name("runtool_btn").click()
