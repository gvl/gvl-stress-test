from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium_snippets.galaxy.history import History
from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets.galaxy import snippet_base

class JoinSubtractGroup(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(JoinSubtractGroup, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_group", "Compare two Datasets")
    def run_compare_two_datasets(self, dataset1, dataset2, col1=None, col2=None):
        driver = self.driver
        self.clock_action("JoinSubtractGroup>Compare Two Datasets run btn click", 1001)
        self.switch_to_galaxy_content_frame()

        driver.find_element_by_xpath("id('s2id_autogen1')/a[contains(@class, 'select2-choice')]").click()
        driver.find_element_by_xpath("//div[contains(@class, 'select2-drop')]/ul/li[contains(., '" + dataset1 + "')][last()]").click()
        if col1:
            Select(driver.find_element_by_name("field1")).select_by_visible_text(col1)

        driver.find_element_by_xpath("id('s2id_autogen3')/a[contains(@class, 'select2-choice')]").click()
        driver.find_element_by_xpath("//div[contains(@class, 'select2-drop')]/ul/li[contains(., '" + dataset2 + "')][last()]").click()
        if col2:
            Select(driver.find_element_by_name("field2")).select_by_visible_text(col2)

        driver.find_element_by_name("runtool_btn").click()

        # wait for cuff diff to finish
        self.clock_action("JoinSubtractGroup>Compare Two Datasets complete", 1001)
